#!/bin/bash

set -e

function download() {

   url=$1
   name=$2

   if [ -f "`pwd`/${name}" ]; then
        ctx logger info "`pwd`/${name} already exists, No need to download"
   else
        # download to given directory
        ctx logger info "Downloading ${url} to `pwd`/${name}"

        set +e
        curl_cmd=$(which curl)
        wget_cmd=$(which wget)
        set -e

        if [[ ! -z ${curl_cmd} ]]; then
            curl -L -o ${name} ${url}
        elif [[ ! -z ${wget_cmd} ]]; then
            wget -O ${name} ${url}
        else
            ctx logger error "Failed to download ${url}: Neither 'cURL' nor 'wget' were found on the system"
            exit 1;
        fi
   fi

}

function untar() {

    tar_archive=$1
    destination=$2

    inner_name=$(tar -tf "${tar_archive}" | grep -o '^[^/]\+' | sort -u)

    if [ ! -d ${destination} ]; then
        ctx logger info "Untaring ${tar_archive}"
        tar -xf ${tar_archive}

        ctx logger info "Moving ${inner_name} to ${destination}"
        mv ${inner_name} ${destination}
    fi
}

TEMP_DIR='/tmp'
TOMCAT_TARBALL_NAME='apache-tomcat-9.0.4.tar.gz'

################################
# Directory that will contain:
#  - Tomcat binaries
################################
TOMCAT_ROOT=${TEMP_DIR}/$(ctx execution-id)/tomcat
TOMCAT_BINARIES_PATH=${TOMCAT_ROOT}/tomcat-binaries
mkdir -p ${TOMCAT_ROOT}

cd ${TEMP_DIR}
download http://apache-mirror.rbc.ru/pub/apache/tomcat/tomcat-9/v9.0.4/bin/${TOMCAT_TARBALL_NAME} ${TOMCAT_TARBALL_NAME}
untar ${TOMCAT_TARBALL_NAME} ${TOMCAT_BINARIES_PATH}

# this runtime property is used by the start-reversenode-app.sh
ctx instance runtime_properties tomcat_binaries_path ${TOMCAT_BINARIES_PATH}

ctx logger info "Sucessfully installed Tomcat"
