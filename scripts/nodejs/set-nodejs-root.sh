#!/bin/bash
ctx logger info "Start setup NodeJS root"
set -e

ctx source instance runtime-properties nodejs_binaries_path $(ctx target instance runtime_properties nodejs_binaries_path)
ctx logger info "Sucessfully setup NodeJS root"
