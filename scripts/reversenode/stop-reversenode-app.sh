#!/bin/bash

set -e

PID=$(ctx instance runtime_properties reversenode_pid)
kill -9 ${PID}
ctx logger info "Sucessfully stopped Reversenode (${PID})"
