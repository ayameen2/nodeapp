#!/bin/bash

set -e

function download() {

  url=$1
  name=$2

  # download to given directory
  ctx logger info "Downloading ${url} to `pwd`/${name}"

  set +e
  curl_cmd=$(which curl)
  wget_cmd=$(which wget)
  set -e

  if [[ ! -z ${curl_cmd} ]]; then
      curl -L -o ${name} ${url}
  elif [[ ! -z ${wget_cmd} ]]; then
      wget -O ${name} ${url}
  else
      ctx logger error "Failed to download ${url}: Neither 'cURL' nor 'wget' were found on the system"
      exit 1;
  fi


}

function extract() {
    set +e
    sudo yum -y install unzip
    set -e

    archive=$1
    destination=$2

    set +e
    unzip_cmd=$(which unzip)
    set -e

    if [[ -z ${unzip_cmd} ]]; then
        ctx logger error "Cannot extract ${archive}: 'unzip' command not found"
        exit 1
    fi
    inner_name=$(unzip -qql "${archive}" | sed -r '1 {s/([ ]+[^ ]+){3}\s+//;q}')
    ctx logger info "Unzipping ${archive}"
    unzip ${archive}

    ctx logger info "Moving ${inner_name} to ${destination}"
    mv ${inner_name} ${destination}

}

TEMP_DIR='/tmp'
NODEJS_BINARIES_PATH=$(ctx instance runtime_properties nodejs_binaries_path)
APPLICATION_URL=$(ctx node properties application_url)
AFTER_SLASH=${APPLICATION_URL##*/}
REVERSENODE_ARCHIVE_NAME=${AFTER_SLASH%%\?*}

################################
# Directory that will contain:
#  - Nodecellar source
################################
REVERSENODE_ROOT_PATH=${TEMP_DIR}/$(ctx execution-id)/reversenode
REVERSENODE_SOURCE_PATH=${REVERSENODE_ROOT_PATH}/reversenode-source
mkdir -p ${REVERSENODE_ROOT_PATH}

cd ${TEMP_DIR}
download ${APPLICATION_URL} ${REVERSENODE_ARCHIVE_NAME}
extract ${REVERSENODE_ARCHIVE_NAME} ${REVERSENODE_SOURCE_PATH}

cd ${REVERSENODE_SOURCE_PATH}
ctx logger info "Installing reversenode dependencies using npm"
${NODEJS_BINARIES_PATH}/bin/npm install

ctx instance runtime_properties reversenode_source_path ${REVERSENODE_SOURCE_PATH}

ctx logger info "Sucessfully installed reversenode application"
