#!/bin/bash

set -e

TOMCAT_BINARIES_PATH=$(ctx instance runtime_properties tomcat_binaries_path)
TOMCAT_SOURCE_PATH=$(ctx instance runtime_properties tomcat_source_path)
STARTUP_SCRIPT=$(ctx node properties startup_script)

COMMAND="${TOMCAT_BINARIES_PATH}/bin/${STARTUP_SCRIPT}"

export TOMCAT_PORT=$(ctx node properties port)

ctx logger info "Starting tomcat application on port ${TOMCAT_PORT}"

ctx logger info "${COMMAND}"
${COMMAND}

# this runtime porperty is used by the stop-reversenode-app.sh script.
ctx instance runtime_properties tomcat_shutdown_cmd "${TOMCAT_BINARIES_PATH}/bin/shutdown.sh"

ctx logger info "Sucessfully started tomcat application"
