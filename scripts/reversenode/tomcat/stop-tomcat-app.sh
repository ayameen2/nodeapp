#!/bin/bash

set -e

tomcat_shutdown_cmd=$(ctx instance runtime_properties tomcat_shutdown_cmd)
$tomcat_shutdown_cmd
ctx logger info "Sucessfully stopped tomcat application (${PID})"
