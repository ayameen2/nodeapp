from setuptools import setup


setup(
    name='hello',
    version='1.0',
    packages=['hello'],
    license='LICENSE',
    description='set hello plugin',
    zip_safe=False,
    install_requires=[
        'cloudify-plugins-common'
    ]
)
