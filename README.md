## NodeApp blueprint
Create Cloudify blueprint does the following:

1. Develop a blueprint for an application that consists of the following:
   1. Compute node called “web”, containing a NodeJS server, listening to port 8081 on
     an interface that is bound to a public IP.
   1. Compute node called “app”, not bound to a public IP, containing a Tomcat server (port 8080).
   1. A simple Java WAR file containing a single resource (say, JSP).
   1. The WAR file has to be deployed to Tomcat.

   1. The resource inside the WAR file should be serveable through NodeJS (i.e.request comes to NodeJS; NodeJS forwards to Tomcat).

2. Upload the blueprint to the Cloudify Manager.
3. Create a single deployment.
4. Demonstrate that the topology works.

## Plugin
1. Create a simple Cloudify plugin that sets a runtime property by the name “hello” on the
current node instance, with the value equal to “world”.
2. Edit the blueprint to contain an “outputs” section, where the value of the “hello” runtime
property is printed.
3. Edit the blueprint to add a custom interface with a custom operation to an existing node,
and bind the plugin operation to that operation.
4. Demonstrate running the custom operation on all instances of the NodeJS node
template.
5. Demonstrate how “cfy deployments outputs” prints the correct value of the “hello”
runtime property.
